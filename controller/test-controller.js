var express = require('express');
var router = express.Router();

const auth = require("../middleware/auth");

router.get('/anonymous', function (req, res) {
    res.send("Hello Anonymous");
});

router.get('/auth', auth, function (req, res) {
    res.send("Hello Auth");
});

module.exports = router;