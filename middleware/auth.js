const jwt = require("jsonwebtoken");
const fs = require('fs')

const config = process.env;

const verifyToken = (req, res, next) => {
    const bearer = req.headers["authorization"];

    if (!bearer) {
        return res.status(401).send("A token is required for authentication");
    }
    try {
        const token = bearer.split(' ')[1];
        var cert = fs.readFileSync('middleware/public.pem');

        const decoded = jwt.verify(token, cert, {
            algorithms: ['RS256'],
            issuer: 'https://app-basf-sso-dev.azurewebsites.net/realms/test-realm'
        });
        req.user = decoded;
    } catch (err) {
        return res.status(401).send("Invalid Token");
    }
    return next();
};

module.exports = verifyToken;